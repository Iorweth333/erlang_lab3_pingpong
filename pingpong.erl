%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. kwi 2017 11:26
%%%-------------------------------------------------------------------
-module(pingpong).
-author("Karol").

%% API
-export([start/0, stop/0, play/1, pong_loop/0, ping_loop/0]).


start () ->
  register (ping, spawn (?MODULE, ping_loop, []) ),
  register (pong, spawn (?MODULE, pong_loop, []) ).


stop () ->
  ping ! stop,
  pong ! stop.

play (N) when is_number(N) ->
  ping ! N.

ping_loop () ->
  receive
    stop -> ok;
    0 -> ping_loop();
    N when is_number(N) -> io:format("Ping odbija ~n"),
      timer:sleep(500),
      pong ! (N-1),
      ping_loop()
  after 20000 -> ok
  end.

pong_loop () ->
  receive
    stop -> ok;
    0 -> pong_loop();
    N when is_number(N) -> io:format("Pong odbija ~n"),
      timer:sleep(500),
      ping ! (N-1),
      pong_loop ()
  after 20000 -> ok

  end.



%  c ("pingpong.erl").
%  pingpong:start().
%  pingpong:play(5).
